<?php

namespace UnpadDev\ElasticApm\Middleware;

use Elastic\Apm\ElasticApm;
use Elastic\Apm\SpanInterface;

class JobMiddleware
{
    public function handle($job, $next)
    {
        if ($this->isEnabled()) {
            $transaction = ElasticApm::getCurrentTransaction();
            $transaction->captureChildSpan(
                'Job Process',
                'job',
                function (SpanInterface $span) use ($next, $job) {
                    $next($job);
                }
            );
        } else {
            $next($job);
        }
    }

    private function isEnabled()
    {
        return (bool) ini_get('elastic_apm.enabled') && class_exists('Elastic\Apm\ElasticApm', false);
    }
}
