<?php

namespace UnpadDev\ElasticApm\Middleware;

use Closure;
use Elastic\Apm\ElasticApm;
use Elastic\Apm\SpanInterface;
use Illuminate\Http\Request;

class ControllerMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $response    = response();
        $transaction = ElasticApm::getCurrentTransaction();
        $transaction->captureChildSpan(
            'Controller Process',
            'controller',
            function (SpanInterface $span) use (&$response, $next, $request) {
                $response = $next($request);
            }
        );

        return $response;
    }
}
