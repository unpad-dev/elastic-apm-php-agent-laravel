<?php

namespace UnpadDev\ElasticApm;

use Elastic\Apm\ElasticApm;
use Illuminate\Console\Events\CommandFinished;
use Illuminate\Console\Events\CommandStarting;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use UnpadDev\ElasticApm\Middleware\ControllerMiddleware;

class ServiceProvider extends BaseServiceProvider
{
    private $enabled = null;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ( ! $this->isEnabled()) return;

        $startTime   = round((defined('LARAVEL_START') ? constant('LARAVEL_START') : microtime(true)) * 1000 * 1000);
        $transaction = ElasticApm::getCurrentTransaction();
        $appBootSpan = $transaction->beginCurrentSpan(
            'App Boot',
            'boot',
            null,
            null,
            $startTime
        );

        $this->app->booting(function() use (&$transaction, &$appBootSpan, &$laravelBootSpan) {
            $appBootSpan->end();
            $laravelBootSpan = $transaction->beginCurrentSpan(
                'Laravel Boot',
                'boot'
            );
        });

        $this->app->booted(function() use (&$laravelBootSpan) {
            $laravelBootSpan->end();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ( ! $this->isEnabled()) return;
        
        if ( ! $this->app->runningInConsole()) {
            $router = $this->app['router'];
            $router->aliasMiddleware('apm', ControllerMiddleware::class);
            $middlewareGroups = array_keys($router->getMiddlewareGroups());
            foreach ($middlewareGroups as $middlewareGroup) {
                $router->prependMiddlewareToGroup($middlewareGroup, ControllerMiddleware::class);
            }
        } else {
            Event::listen(function(CommandStarting $event) {
                $transaction = ElasticApm::getCurrentTransaction();
                $transaction->setName($this->getCommand());
                $transaction->setType('cli');
                
                if ($this->isLongQueueJob()) {
                    $transaction->end();
                }
            });

            Event::listen(function(CommandFinished $event) {
                $transaction = ElasticApm::getCurrentTransaction();
                $transaction->end();
            });

            Event::listen(function(JobProcessing $event) {
                if ( ! $this->isLongQueueJob()) {                    
                    $transaction = ElasticApm::getCurrentTransaction();
                    $transaction->end();
                }

                ElasticApm::beginCurrentTransaction(data_get($event->job->payload(), 'displayName'), 'job');
            });

            Event::listen(function(JobProcessed $event) {
                $transaction = ElasticApm::getCurrentTransaction();
                $transaction->setResult('Job Processed');
                $transaction->end();
            });

            Event::listen(function(JobFailed $event) {
                $transaction = ElasticApm::getCurrentTransaction();
                $transaction->setResult('Job Failed');
                $transaction->tracer()->createErrorFromThrowable($event->exception);
                $transaction->end();
            });
        }
    }

    private function isEnabled()
    {
        $this->enabled = $this->enabled ?? (bool) ini_get('elastic_apm.enabled') && class_exists('Elastic\Apm\ElasticApm', false);
        
        return $this->enabled;
    }

    private function getCommand()
    {
        $argv    = (array) data_get($_SERVER, 'argv', []);
        $argv[0] = pathinfo(data_get($argv, 0), PATHINFO_FILENAME);

        return implode(' ', $argv);
    }

    private function isLongQueueJob()
    {
        $argv = (array) data_get($_SERVER, 'argv', []);

        return data_get($argv, 1) == 'queue:work' && ! in_array('--once', $argv);
    }
}
