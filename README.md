# Elastic APM PHP Agent - Laravel

Additional Features for [Elastic APM PHP Agent](https://www.elastic.co/guide/en/apm/agent/php/1.x/setup.html) when using [Laravel 9+](https://laravel.com/docs/9.x) Framework

## Features
- **Artisan Transaction Name** with arguments and options
- **Job Transaction** for `queue:work` and `queue:listen`
- **Job Process** in **Job Transaction**
- **Controller Process** in **Request Transaction**

## Installation
#### 1. Install Elastic APM PHP Agent
##### Debian 11
1. Install [Elastic APM PHP Agent](https://www.elastic.co/guide/en/apm/agent/php/1.x/setup.html) according to the instructions at `https://www.elastic.co/guide/en/apm/agent/php/1.x/setup.html`
2. Configure APM PHP Agent until it appears in `phpinfo()` (PHP FPM) and also appears in `php --ini` (PHP Cli)
3. Make sure the configuration works using [Kibana](https://www.elastic.co/kibana/) (which is connected to the Elastic APM Server) on the `Observability - APM` menu where the `elastic_apm.service_name` value in the php ini file is shown as a **Service**
##### Docker
1. Use [unpad-nginx-php8.1](https://hub.docker.com/r/rafee12/unpad-nginx-php8.1) Docker Image with version 1.1+
2. Make sure to use this Environments below to running the docker container

| Env | Value | Description |
| ----------- | ----------- | ----------- |
| APM_ENABLED | true | Elastic APM PHP Agent Status |
| APM_SERVER_URL | http://elastic-apm-host:8200 | URL for [Elastic APM](https://www.elastic.co/guide/en/apm/guide/8.6/index.html) server |
| APM_SERVICE_NAME | App Name | The application name |

3. Make sure the docker environments works using [Kibana](https://www.elastic.co/kibana/) (which is connected to the Elastic APM Server) on the `Observability - APM` menu where the `APM_SERVICE_NAME` value in the container environment is shown as a **Service**

#### 2. Install Package
Once the PHP APM Elastic Agent is installed, install this package using composer
```
$ composer require unpad-dev/elastic-apm-php-agent-laravel
```

## Usage for Job Process

Attach Middleware `\UnpadDev\ElasticApm\Middleware\JobMiddleware` to every [Job](https://laravel.com/docs/9.x/queues#creating-jobs) in the job's `middleware` method to show **Job Process** in Elastic APM
```php
use UnpadDev\ElasticApm\Middleware\JobMiddleware;

/**
* Get the middleware the job should pass through.
*
* @return array
*/
public function middleware()
{
    return [new JobMiddleware];
}
```